#! /usr/bin/python

import argparse
import i3ipc

parser = argparse.ArgumentParser()
parser.add_argument('--move', action='store_true')
args = parser.parse_args()

i3 = i3ipc.Connection()
work_nums = sorted([x.num for x in i3.get_workspaces() if x.num != None and x.num >= 0])
next_free_num = 1
for i in work_nums:
    if i != next_free_num:
        break
    next_free_num += 1
if args.move:
    i3.command('move container to workspace ' + str(next_free_num))
i3.command('workspace ' + str(next_free_num))
