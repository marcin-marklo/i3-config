#! /bin/bash

# Theme
THEME_NAME="Arc"
FONT_NAME="DejaVu Sans 8"
ICON_NAME="Faenza Dark"

# get latest i3
sudo echo "deb http://debian.sur5r.net/i3/ $(lsb_release -c -s) universe" >> /etc/apt/sources.list
sudo apt-get update
sudo apt-get --allow-unauthenticated install sur5r-keyring
sudo apt-get update

#setup i3
sudo apt-get install -y \
     i3 \
     python-pip \
     gettext \
     feh \
     network-manager-gnome \
     pcmanfm \
     solaar \
     faenza-icon-theme \
     fonts-font-awesome
sudo pip install \
     i3ipc \
     udiskie
mkdir ~/.i3
cp * ~/.i3/
mv ~/.i3/i3status.conf ~/.i3status.conf
mkdir -p ~/Pictures
ln -sf $(pwd)/wallpaper.png ~/Pictures/.wallpaper

# setup theme
sudo apt-get install -y \
     gnome-themes-standard \
     gtk2-engines-murrine \
     autoconf \
     automake \
     pkg-config \
     libgtk-3-dev
pushd ..
git clone https://github.com/horst3180/arc-theme --depth 1
cd arc-theme
./autogen.sh --prefix=/usr
sudo make install
echo gtk-theme-name = "$THEME_NAME" > ~/.gtkrc-2.0
echo gtk-font-name = "$FONT_NAME" >> ~/.gtkrc-2.0
echo gtk-icon-theme-name = "$ICON_NAME" >> ~/.gtkrc-2.0
echo '[Settings]' > ~/.config/gtk-3.0/settings.ini
echo gtk-theme-name = $THEME_NAME >> ~/.config/gtk-3.0/settings.ini
echo gtk-font-name = $FONT_NAME >> ~/.config/gtk-3.0/settings.ini
echo gtk-icon-theme-name = $ICON_NAME >> ~/.config/gtk-3.0/settings.ini
popd
